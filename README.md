# terraform-state

## Summary

This module creates an S3 bucket and DynamoDB table to be used in terraform for managing state data.

## Resources

This module creates the following resources:

* [aws_dynamodb_table](https://www.terraform.io/docs/providers/aws/r/dynamodb_table.html)
* [aws_s3_bucket](https://www.terraform.io/docs/providers/aws/r/s3_bucket.html)
* [aws_s3_bucket_public_access_block](https://www.terraform.io/docs/providers/aws/r/s3_bucket_public_access_block.html)
* [aws_s3_bucket_policy](https://www.terraform.io/docs/providers/aws/r/s3_bucket_policy.html)

## Cost

Use of this module will result in billable resources being launched in AWS. Before using this module, please familiarize yourself with the expenses associated with S3 buckets and DynamoDB tables.

* AWS S3 pricing information is available [here](https://aws.amazon.com/s3/pricing/)
* AWS DynamoDB pricing information is available [here](https://aws.amazon.com/dynamodb/pricing/)

## Inputs

| Name              | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Type          | Default          | Required |
|:------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:-----------------|:---------|
| lock_table_name   | The name of a DynamoDB table to use for state locking and consistency.                                                                                                                                                                                                                                                                                                                                                                                           | `string`      | `terraform-lock` | no       |
| state_bucket_name | The name of the S3 bucket to use for storing terraform state. Must be globally unique.                                                                                                                                                                                                                                                                                                                                                                           | `string`      | -                | yes      |
| kms_key_id        | The AWS KMS master key ID used for the SSE-KMS encryption on the terraform state S3 bucket. If the kms_key_id is specified, the bucket default encryption key management method will be set to aws-kms. If the kms_key_id is not specified (the default), then the default encryption key management method will be set to aes-256 (also known as AWS-S3 key management). The default AWS/S3 AWS KMS master key is used if this element is absent (the default). | `string`      | -                | no       |
| tags              | Additional tags to attach to all resources created by this module.                                                                                                                                                                                                                                                                                                                                                                                               | `map(string)` | -                | no       |

## Outputs

| Name            | Description                                                          |
|:----------------|:---------------------------------------------------------------------|
| lock_table_id   | The ID of the DynamoDB table used for state locking and consistency. |
| state_bucket_id | The ID of the S3 bucket used for storing terraform state.            |

## Examples

### Simple Usage

```terraform
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.42"
}

module "terraform_state" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/terraform-state.git?ref=v1"

  state_bucket_name = "example-terraform-state"
}
```

## Version History

* v1 - Initial Release
