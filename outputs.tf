output "lock_table_id" {
  description = "The ID of the DynamoDB table used for state locking and consistency."
  value       = aws_dynamodb_table.lock_table.id
}

output "state_bucket_id" {
  description = "The ID of the S3 bucket used for storing terraform state."
  value       = aws_s3_bucket.state_bucket.id
}
