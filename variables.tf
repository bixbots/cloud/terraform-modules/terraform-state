variable "lock_table_name" {
  type        = string
  default     = "terraform-lock"
  description = "(Optional; Default: terraform-lock) The name of a DynamoDB table to use for state locking and consistency."
}

variable "state_bucket_name" {
  type        = string
  description = "The name of the S3 bucket to use for storing terraform state."
}

variable "kms_key_id" {
  type        = string
  default     = ""
  description = "(Optional) The AWS KMS master key ID used for the SSE-KMS encryption on the tf state s3 bucket. If the kms_key_id is specified, the bucket default encryption key management method will be set to aws-kms. If the kms_key_id is not specified (the default), then the default encryption key management method will be set to aes-256 (also known as aws-s3 key management). The default aws/s3 AWS KMS master key is used if this element is absent (the default)."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
